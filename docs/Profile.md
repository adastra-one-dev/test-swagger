# JdemekousatczApiDocumentation.Profile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifyEnabled** | **Boolean** | Should be a user notified on vaccination expiration. | [optional] 
**notifyHow** | **String** | How should be a user notified. Default is e-mail. | [optional] 
**notifyWhen** | [**ModelObject**](ModelObject.md) | When in days should be a user notified before expiration. Default is 7 days (week). | [optional] 

<a name="NotifyHowEnum"></a>
## Enum: NotifyHowEnum

* `email` (value: `"email"`)
* `sms` (value: `"sms"`)

