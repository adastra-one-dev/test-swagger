# JdemekousatczApiDocumentation.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Internal user id. | [optional] 
**name** | **String** | Full name. | [optional] 
**email** | **String** | Contact email important for using as loginname, cannot be changed anymore. | [optional] 
**createdAt** | **Date** | Day of an account creating. | [optional] 
**role** | **String** | User role in system. | [optional] 
**settings** | [**Profile**](Profile.md) |  | [optional] 

<a name="RoleEnum"></a>
## Enum: RoleEnum

* `guest` (value: `"guest"`)
* `user` (value: `"user"`)
* `kennel` (value: `"kennel"`)

