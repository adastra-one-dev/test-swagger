# JdemekousatczApiDocumentation.Body

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**vaccinatedAt** | **Date** |  | [optional] 
**expireAt** | **Date** |  | [optional] 

<a name="TypeEnum"></a>
## Enum: TypeEnum

* `rabbies` (value: `"rabbies"`)
* `tetanus` (value: `"tetanus"`)
* `infectious-disease` (value: `"infectious-disease"`)
* `kennel-cough` (value: `"kennel-cough"`)
* `lyme-disease` (value: `"lyme-disease"`)
* `worm-treatment` (value: `"worm-treatment"`)
* `others` (value: `"others"`)

