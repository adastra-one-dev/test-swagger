# JdemekousatczApiDocumentation.Body3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifyEnabled** | **Boolean** |  | [optional] 
**notifyHow** | **String** |  | [optional] 
**notifyWhen** | [**Numeric**](Numeric.md) |  | [optional] 

<a name="NotifyHowEnum"></a>
## Enum: NotifyHowEnum

* `email` (value: `"email"`)
* `sms` (value: `"sms"`)

