# JdemekousatczApiDocumentation.Pet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Internal pet id | [optional] 
**family** | [**ModelEnum**](ModelEnum.md) |  | [optional] 
**gender** | [**ModelEnum**](ModelEnum.md) |  | [optional] 
**name** | **String** | Name (official) of pet | [optional] 
**nickname** | **String** | Pets nickname | [optional] 
**bornAt** | **String** | Date of birth in format yyyy-mm-dd | [optional] 
