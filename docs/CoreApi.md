# JdemekousatczApiDocumentation.CoreApi

All URIs are relative to *http://www.jdemekousat.loc/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userGet**](CoreApi.md#userGet) | **GET** /user | Detail info on currently logged user.

<a name="userGet"></a>
# **userGet**
> User userGet()

Detail info on currently logged user.

### Example
```javascript
import JdemekousatczApiDocumentation from 'jdemekousatcz_api_documentation';

let apiInstance = new JdemekousatczApiDocumentation.CoreApi();
apiInstance.userGet((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

