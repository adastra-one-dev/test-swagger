# JdemekousatczApiDocumentation.Vaccine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**petId** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**vaccinatedAt** | **Date** |  | [optional] 
**expireAt** | **Date** | Can be null when vaccine does not expire | [optional] 
**isValid** | **Boolean** |  | [optional] 

<a name="TypeEnum"></a>
## Enum: TypeEnum

* `rabbies` (value: `"rabbies"`)

