# JdemekousatczApiDocumentation.Body2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Official name of pet (usually from registration licence) | [optional] 
**nickname** | **String** | Call name of pet | [optional] 
**gender** | **String** | Gender | [optional] 
**family** | **String** | Family | [optional] 
**bornAt** | **String** | Day of birth in format YYYY-mm-dd | [optional] 

<a name="GenderEnum"></a>
## Enum: GenderEnum

* `F` (value: `"F"`)
* `G` (value: `"G"`)


<a name="FamilyEnum"></a>
## Enum: FamilyEnum

* `cat` (value: `"cat"`)
* `dog` (value: `"dog"`)
* `ferret` (value: `"ferret"`)
* `horse` (value: `"horse"`)

