/**
 * Jdemekousat.cz api documentation
 * Jdemekousat.cz api documentation
 *
 * OpenAPI spec version: 20200808-001
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.JdemekousatczApiDocumentation);
  }
}(this, function(expect, JdemekousatczApiDocumentation) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new JdemekousatczApiDocumentation.Body3();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Body3', function() {
    it('should create an instance of Body3', function() {
      // uncomment below and update the code to test Body3
      //var instane = new JdemekousatczApiDocumentation.Body3();
      //expect(instance).to.be.a(JdemekousatczApiDocumentation.Body3);
    });

    it('should have the property notifyEnabled (base name: "notify_enabled")', function() {
      // uncomment below and update the code to test the property notifyEnabled
      //var instane = new JdemekousatczApiDocumentation.Body3();
      //expect(instance).to.be();
    });

    it('should have the property notifyHow (base name: "notify_how")', function() {
      // uncomment below and update the code to test the property notifyHow
      //var instane = new JdemekousatczApiDocumentation.Body3();
      //expect(instance).to.be();
    });

    it('should have the property notifyWhen (base name: "notify_when")', function() {
      // uncomment below and update the code to test the property notifyWhen
      //var instane = new JdemekousatczApiDocumentation.Body3();
      //expect(instance).to.be();
    });

  });

}));
