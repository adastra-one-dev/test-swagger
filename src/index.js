/**
 * Jdemekousat.cz api documentation
 * Jdemekousat.cz api documentation
 *
 * OpenAPI spec version: 20200808-001
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from './ApiClient';
import User from './model/User';
import CoreApi from './api/CoreApi';

/**
* Jdemekousat_cz_api_documentation.<br>
* The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
* <p>
* An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
* <pre>
* var JdemekousatczApiDocumentation = require('index'); // See note below*.
* var xxxSvc = new JdemekousatczApiDocumentation.XxxApi(); // Allocate the API class we're going to use.
* var yyyModel = new JdemekousatczApiDocumentation.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
* and put the application logic within the callback function.</em>
* </p>
* <p>
* A non-AMD browser application (discouraged) might do something like this:
* <pre>
* var xxxSvc = new JdemekousatczApiDocumentation.XxxApi(); // Allocate the API class we're going to use.
* var yyy = new JdemekousatczApiDocumentation.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* </p>
* @module index
* @version 20200808-001
*/
export {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient,

    /**
     * The User model constructor.
     * @property {module:model/User}
     */
    User,

    /**
    * The CoreApi service constructor.
    * @property {module:api/CoreApi}
    */
    CoreApi
};
